<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05624">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation for adjourning the Parliament from the fifteenth of March instant, until the fifteenth of June next.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05624 of text R183490 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing S1818). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05624</idno>
    <idno type="STC">Wing S1818</idno>
    <idno type="STC">ESTC R183490</idno>
    <idno type="EEBO-CITATION">52529289</idno>
    <idno type="OCLC">ocm 52529289</idno>
    <idno type="VID">179057</idno>
    <idno type="PROQUESTGOID">2240898528</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05624)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179057)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2776:46)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation for adjourning the Parliament from the fifteenth of March instant, until the fifteenth of June next.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1689-1694 : William and Mary)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the successors of Andrew Anderson, Printer to their most excellent Majesties,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1694.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms in ornamental border at head of text; initial letter.</note>
      <note>Dated: Given under Our Signet at Edinburgh, the twelfth day of March, and of Our Reign the fifth year, 1694.</note>
      <note>Signed: Gilb. Eliot, Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1689-1745 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation : for adjourning the Parliament from the fifteenth of March instant, until the fifteenth of June next.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1694</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>421</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-01</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05624-t">
  <body xml:id="B05624-e0">
   <div type="royal_proclamation" xml:id="B05624-e10">
    <pb facs="tcp:179057:1" rend="simple:additions" xml:id="B05624-001-a"/>
    <head xml:id="B05624-e20">
     <figure xml:id="B05624-e30">
      <figDesc xml:id="B05624-e40">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B05624-e50">
     <hi xml:id="B05624-e60">
      <w lemma="a" pos="d" xml:id="B05624-001-a-0010">A</w>
      <w lemma="proclamation" pos="n1" xml:id="B05624-001-a-0020">PROCLAMATION</w>
     </hi>
     <w lemma="for" pos="acp" xml:id="B05624-001-a-0030">For</w>
     <w lemma="adjourn" pos="vvg" xml:id="B05624-001-a-0040">Adjourning</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-0050">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-0060">Parliament</w>
     <w lemma="from" pos="acp" xml:id="B05624-001-a-0070">from</w>
     <date xml:id="B05624-e70">
      <w lemma="the" pos="d" xml:id="B05624-001-a-0080">the</w>
      <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-0090">fifteenth</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0100">of</w>
      <hi xml:id="B05624-e80">
       <w lemma="march" pos="n1" xml:id="B05624-001-a-0110">March</w>
      </hi>
      <w lemma="instant" pos="n1-j" xml:id="B05624-001-a-0120">instant</w>
      <pc xml:id="B05624-001-a-0130">,</pc>
      <w lemma="until" pos="acp" xml:id="B05624-001-a-0140">until</w>
      <w lemma="the" pos="d" xml:id="B05624-001-a-0150">the</w>
      <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-0160">fifteenth</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0170">of</w>
      <hi xml:id="B05624-e90">
       <w lemma="June" pos="nn1" xml:id="B05624-001-a-0180">June</w>
      </hi>
      <w lemma="next" pos="ord" xml:id="B05624-001-a-0190">next</w>
      <pc unit="sentence" xml:id="B05624-001-a-0200">.</pc>
     </date>
    </head>
    <opener xml:id="B05624-e100">
     <signed xml:id="B05624-e110">
      <hi xml:id="B05624-e120">
       <w lemma="william" pos="nn1" rend="decorinit" xml:id="B05624-001-a-0210">WILLIAM</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05624-001-a-0220">and</w>
      <hi xml:id="B05624-e130">
       <w lemma="MARY" pos="nn1" xml:id="B05624-001-a-0230">MARY</w>
      </hi>
      <w lemma="by" pos="acp" xml:id="B05624-001-a-0240">by</w>
      <w lemma="the" pos="d" xml:id="B05624-001-a-0250">the</w>
      <w lemma="grace" pos="n1" xml:id="B05624-001-a-0260">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0270">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B05624-001-a-0280">GOD</w>
      <pc xml:id="B05624-001-a-0290">,</pc>
      <w lemma="king" pos="n1" xml:id="B05624-001-a-0300">King</w>
      <w lemma="and" pos="cc" xml:id="B05624-001-a-0310">and</w>
      <w lemma="queen" pos="n1" xml:id="B05624-001-a-0320">Queen</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0330">of</w>
      <hi xml:id="B05624-e140">
       <w lemma="great-britain" pos="nn1" xml:id="B05624-001-a-0340">Great-Britain</w>
       <pc xml:id="B05624-001-a-0350">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05624-001-a-0360">France</w>
       <pc xml:id="B05624-001-a-0370">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05624-001-a-0380">and</w>
      <hi xml:id="B05624-e150">
       <w lemma="Ireland" pos="nn1" xml:id="B05624-001-a-0390">Ireland</w>
       <pc xml:id="B05624-001-a-0400">,</pc>
      </hi>
      <w lemma="defender" pos="n2" xml:id="B05624-001-a-0410">Defenders</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0420">of</w>
      <w lemma="the" pos="d" xml:id="B05624-001-a-0430">the</w>
      <w lemma="Faith" pos="n1" xml:id="B05624-001-a-0440">Faith</w>
      <pc unit="sentence" xml:id="B05624-001-a-0441">.</pc>
      <pc unit="sentence" xml:id="B05624-001-a-0450"/>
     </signed>
     <salute xml:id="B05624-e160">
      <w lemma="to" pos="prt" xml:id="B05624-001-a-0460">To</w>
      <w lemma="our" pos="po" xml:id="B05624-001-a-0470">Our</w>
      <w lemma="lion" pos="n1" reg="Lion" xml:id="B05624-001-a-0480">Lyon</w>
      <w lemma="king" pos="n1" xml:id="B05624-001-a-0490">King</w>
      <w lemma="at" pos="acp" xml:id="B05624-001-a-0500">at</w>
      <w lemma="arm" pos="n2" xml:id="B05624-001-a-0510">Arms</w>
      <pc xml:id="B05624-001-a-0520">,</pc>
      <w lemma="and" pos="cc" xml:id="B05624-001-a-0530">and</w>
      <w lemma="his" pos="po" xml:id="B05624-001-a-0540">his</w>
      <w lemma="brethren" pos="n2" xml:id="B05624-001-a-0550">Brethren</w>
      <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05624-001-a-0560">Heraulds</w>
      <pc xml:id="B05624-001-a-0570">,</pc>
      <w lemma="macer" pos="n2" xml:id="B05624-001-a-0580">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05624-001-a-0590">of</w>
      <w lemma="our" pos="po" xml:id="B05624-001-a-0600">Our</w>
      <w lemma="privy" pos="j" xml:id="B05624-001-a-0610">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05624-001-a-0620">Council</w>
      <pc xml:id="B05624-001-a-0630">,</pc>
      <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05624-001-a-0640">Pursevants</w>
      <pc xml:id="B05624-001-a-0650">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05624-001-a-0660">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05624-001-a-0670">at</w>
      <w lemma="arm" pos="n2" xml:id="B05624-001-a-0680">Arms</w>
      <pc xml:id="B05624-001-a-0690">,</pc>
      <w lemma="our" pos="po" xml:id="B05624-001-a-0700">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05624-001-a-0710">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05624-001-a-0720">in</w>
      <w lemma="that" pos="d" xml:id="B05624-001-a-0730">that</w>
      <w lemma="part" pos="n1" xml:id="B05624-001-a-0740">part</w>
      <pc xml:id="B05624-001-a-0750">,</pc>
      <w lemma="conjunct" pos="av-j" xml:id="B05624-001-a-0760">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05624-001-a-0770">and</w>
      <w lemma="several" pos="av-j" xml:id="B05624-001-a-0780">Severally</w>
      <pc xml:id="B05624-001-a-0790">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05624-001-a-0800">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05624-001-a-0810">constitute</w>
      <pc xml:id="B05624-001-a-0820">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05624-001-a-0830">Greeting</w>
      <pc xml:id="B05624-001-a-0840">:</pc>
     </salute>
    </opener>
    <p xml:id="B05624-e170">
     <w lemma="forasmuch" pos="av" xml:id="B05624-001-a-0850">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05624-001-a-0860">as</w>
     <pc xml:id="B05624-001-a-0870">,</pc>
     <w lemma="we" pos="pns" xml:id="B05624-001-a-0880">We</w>
     <w lemma="with" pos="acp" xml:id="B05624-001-a-0890">with</w>
     <w lemma="advice" pos="n1" xml:id="B05624-001-a-0900">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-0910">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-0920">the</w>
     <w lemma="lord" pos="n2" xml:id="B05624-001-a-0930">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-0940">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-0950">Our</w>
     <w lemma="privy" pos="j" xml:id="B05624-001-a-0960">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05624-001-a-0970">Council</w>
     <pc xml:id="B05624-001-a-0980">,</pc>
     <w lemma="do" pos="vvd" xml:id="B05624-001-a-0990">Did</w>
     <w lemma="by" pos="acp" xml:id="B05624-001-a-1000">by</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-1010">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B05624-001-a-1020">Proclamation</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1030">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1040">the</w>
     <w lemma="date" pos="n1" xml:id="B05624-001-a-1050">date</w>
     <pc xml:id="B05624-001-a-1060">,</pc>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1070">the</w>
     <w lemma="twenty" pos="crd" xml:id="B05624-001-a-1080">twenty</w>
     <w lemma="six" pos="crd" xml:id="B05624-001-a-1090">six</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-1100">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1110">of</w>
     <hi xml:id="B05624-e180">
      <w lemma="December" pos="nn1" xml:id="B05624-001-a-1120">December</w>
     </hi>
     <w lemma="one" pos="crd" xml:id="B05624-001-a-1130">one</w>
     <w lemma="thousand" pos="crd" xml:id="B05624-001-a-1140">thousand</w>
     <w lemma="six" pos="crd" xml:id="B05624-001-a-1150">six</w>
     <w lemma="hundred" pos="crd" xml:id="B05624-001-a-1160">hundred</w>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-1170">and</w>
     <w lemma="ninety" pos="crd" xml:id="B05624-001-a-1180">ninety</w>
     <w lemma="three" pos="crd" xml:id="B05624-001-a-1190">three</w>
     <w lemma="year" pos="n2" xml:id="B05624-001-a-1200">years</w>
     <pc xml:id="B05624-001-a-1210">,</pc>
     <w lemma="adjourn" pos="vvb" xml:id="B05624-001-a-1220">Adjourn</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1230">the</w>
     <w lemma="current" pos="j" xml:id="B05624-001-a-1240">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-1250">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1260">of</w>
     <w lemma="this" pos="d" xml:id="B05624-001-a-1270">this</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-1280">Our</w>
     <w lemma="ancient" pos="j" reg="Ancient" xml:id="B05624-001-a-1290">Antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05624-001-a-1300">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1310">of</w>
     <hi xml:id="B05624-e190">
      <w lemma="Scotland" pos="nn1" xml:id="B05624-001-a-1320">Scotland</w>
      <pc xml:id="B05624-001-a-1330">,</pc>
     </hi>
     <w lemma="from" pos="acp" xml:id="B05624-001-a-1340">from</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1350">the</w>
     <w lemma="nine" pos="ord" xml:id="B05624-001-a-1360">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-1370">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1380">of</w>
     <hi xml:id="B05624-e200">
      <w lemma="January" pos="nn1" xml:id="B05624-001-a-1390">January</w>
     </hi>
     <w lemma="last" pos="ord" xml:id="B05624-001-a-1400">last</w>
     <w lemma="by" pos="acp" xml:id="B05624-001-a-1410">by</w>
     <w lemma="past" pos="j" xml:id="B05624-001-a-1420">past</w>
     <pc xml:id="B05624-001-a-1430">,</pc>
     <w lemma="to" pos="prt" xml:id="B05624-001-a-1440">to</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1450">the</w>
     <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-1460">Fifteenth</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-1470">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1480">of</w>
     <hi xml:id="B05624-e210">
      <w lemma="March." pos="nn1" xml:id="B05624-001-a-1490">March.</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-1500">And</w>
     <w lemma="we" pos="pns" xml:id="B05624-001-a-1510">We</w>
     <w lemma="consider" pos="vvg" xml:id="B05624-001-a-1520">Considering</w>
     <pc xml:id="B05624-001-a-1530">,</pc>
     <w lemma="that" pos="cs" xml:id="B05624-001-a-1540">that</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1550">the</w>
     <w lemma="present" pos="j" xml:id="B05624-001-a-1560">present</w>
     <w lemma="state" pos="n1" xml:id="B05624-001-a-1570">State</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1580">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-1590">Our</w>
     <w lemma="affair" pos="n2" xml:id="B05624-001-a-1600">Affairs</w>
     <w lemma="in" pos="acp" xml:id="B05624-001-a-1610">in</w>
     <w lemma="this" pos="d" xml:id="B05624-001-a-1620">this</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-1630">Our</w>
     <w lemma="ancient" pos="j" reg="Ancient" xml:id="B05624-001-a-1640">Antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05624-001-a-1650">Kingdom</w>
     <pc xml:id="B05624-001-a-1660">,</pc>
     <w lemma="do" pos="vvz" xml:id="B05624-001-a-1670">doth</w>
     <w lemma="not" pos="xx" xml:id="B05624-001-a-1680">not</w>
     <w lemma="require" pos="vvi" xml:id="B05624-001-a-1690">Require</w>
     <w lemma="that" pos="cs" xml:id="B05624-001-a-1700">that</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1710">the</w>
     <w lemma="member" pos="n2" xml:id="B05624-001-a-1720">Members</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-1730">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-1740">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05624-001-a-1750">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-1760">Parliament</w>
     <w lemma="shall" pos="vmd" xml:id="B05624-001-a-1770">should</w>
     <w lemma="meet" pos="vvi" xml:id="B05624-001-a-1780">meet</w>
     <w lemma="upon" pos="acp" xml:id="B05624-001-a-1790">upon</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-1800">the</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05624-001-a-1810">foresaid</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-1820">day</w>
     <pc unit="sentence" xml:id="B05624-001-a-1830">.</pc>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-1840">And</w>
     <w lemma="we" pos="pns" xml:id="B05624-001-a-1850">We</w>
     <w lemma="be" pos="vvg" xml:id="B05624-001-a-1860">being</w>
     <w lemma="unwilling" pos="j" xml:id="B05624-001-a-1870">unwilling</w>
     <pc xml:id="B05624-001-a-1880">,</pc>
     <w lemma="that" pos="cs" xml:id="B05624-001-a-1890">that</w>
     <w lemma="they" pos="pns" xml:id="B05624-001-a-1900">they</w>
     <w lemma="shall" pos="vmd" xml:id="B05624-001-a-1910">should</w>
     <w lemma="be" pos="vvi" xml:id="B05624-001-a-1920">be</w>
     <w lemma="put" pos="vvn" xml:id="B05624-001-a-1930">put</w>
     <w lemma="to" pos="acp" xml:id="B05624-001-a-1940">to</w>
     <w lemma="any" pos="d" xml:id="B05624-001-a-1950">any</w>
     <w lemma="trouble" pos="vvb" xml:id="B05624-001-a-1960">Trouble</w>
     <pc xml:id="B05624-001-a-1970">,</pc>
     <w lemma="that" pos="cs" xml:id="B05624-001-a-1980">that</w>
     <w lemma="may" pos="vmb" xml:id="B05624-001-a-1990">may</w>
     <w lemma="be" pos="vvi" xml:id="B05624-001-a-2000">be</w>
     <w lemma="dispense" pos="vvn" xml:id="B05624-001-a-2010">Dispensed</w>
     <w lemma="with" pos="acp" xml:id="B05624-001-a-2020">with</w>
     <pc xml:id="B05624-001-a-2030">;</pc>
     <w lemma="do" pos="vvb" xml:id="B05624-001-a-2040">Do</w>
     <w lemma="therefore" pos="av" xml:id="B05624-001-a-2050">therefore</w>
     <w lemma="with" pos="acp" xml:id="B05624-001-a-2060">with</w>
     <w lemma="advice" pos="n1" xml:id="B05624-001-a-2070">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2080">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2090">the</w>
     <w lemma="say" pos="ng1-vn" reg="said's" xml:id="B05624-001-a-2100">saids</w>
     <w lemma="lord" pos="n2" xml:id="B05624-001-a-2110">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2120">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-2130">Our</w>
     <w lemma="privy" pos="j" xml:id="B05624-001-a-2140">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05624-001-a-2150">Council</w>
     <pc xml:id="B05624-001-a-2160">,</pc>
     <w lemma="hereby" pos="av" xml:id="B05624-001-a-2170">hereby</w>
     <w lemma="adjourn" pos="vvb" xml:id="B05624-001-a-2180">Adjourn</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-2190">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05624-001-a-2200">said</w>
     <w lemma="current" pos="n1" xml:id="B05624-001-a-2210">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-2220">Parliament</w>
     <w lemma="until" pos="acp" xml:id="B05624-001-a-2230">until</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2240">the</w>
     <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-2250">fifteenth</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-2260">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2270">of</w>
     <hi xml:id="B05624-e220">
      <w lemma="June" pos="nn1" xml:id="B05624-001-a-2280">June</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="B05624-001-a-2290">next</w>
     <w lemma="to" pos="prt" xml:id="B05624-001-a-2300">to</w>
     <w lemma="come" pos="vvi" xml:id="B05624-001-a-2310">come</w>
     <pc xml:id="B05624-001-a-2320">:</pc>
     <w lemma="hereby" pos="av" xml:id="B05624-001-a-2330">Hereby</w>
     <w lemma="require" pos="vvg" xml:id="B05624-001-a-2340">Requiring</w>
     <w lemma="all" pos="d" xml:id="B05624-001-a-2350">all</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2360">the</w>
     <w lemma="member" pos="n2" xml:id="B05624-001-a-2370">Members</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2380">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-2390">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-2400">Parliament</w>
     <w lemma="to" pos="prt" xml:id="B05624-001-a-2410">to</w>
     <w lemma="attend" pos="vvi" xml:id="B05624-001-a-2420">attend</w>
     <w lemma="that" pos="d" xml:id="B05624-001-a-2430">that</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-2440">day</w>
     <pc xml:id="B05624-001-a-2450">,</pc>
     <w lemma="in" pos="acp" xml:id="B05624-001-a-2460">in</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2470">the</w>
     <w lemma="usual" pos="j" xml:id="B05624-001-a-2480">usual</w>
     <w lemma="way" pos="n1" xml:id="B05624-001-a-2490">way</w>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-2500">and</w>
     <w lemma="under" pos="acp" xml:id="B05624-001-a-2510">under</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2520">the</w>
     <w lemma="certification" pos="n2" xml:id="B05624-001-a-2530">Certifications</w>
     <w lemma="contain" pos="vvn" xml:id="B05624-001-a-2540">contained</w>
     <w lemma="in" pos="acp" xml:id="B05624-001-a-2550">in</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2560">the</w>
     <w lemma="several" pos="j" xml:id="B05624-001-a-2570">several</w>
     <w lemma="act" pos="n2" xml:id="B05624-001-a-2580">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2590">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-2600">Parliament</w>
     <w lemma="madethereanent" pos="fla" xml:id="B05624-001-a-2610">madethereanent</w>
     <pc unit="sentence" xml:id="B05624-001-a-2620">.</pc>
     <w lemma="our" pos="po" xml:id="B05624-001-a-2630">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05624-001-a-2640">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05624-001-a-2650">IS</w>
     <w lemma="herefore" pos="av" xml:id="B05624-001-a-2660">HEREFORE</w>
     <pc xml:id="B05624-001-a-2670">,</pc>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-2680">and</w>
     <w lemma="we" pos="pns" xml:id="B05624-001-a-2690">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05624-001-a-2700">Charge</w>
     <w lemma="you" pos="pn" xml:id="B05624-001-a-2710">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05624-001-a-2720">strictly</w>
     <pc xml:id="B05624-001-a-2730">,</pc>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-2740">and</w>
     <w lemma="command" pos="vvb" xml:id="B05624-001-a-2750">Command</w>
     <pc xml:id="B05624-001-a-2760">,</pc>
     <w lemma="that" pos="cs" xml:id="B05624-001-a-2770">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05624-001-a-2780">incontinent</w>
     <w lemma="these" pos="d" xml:id="B05624-001-a-2790">these</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-2800">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05624-001-a-2810">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05624-001-a-2820">seen</w>
     <pc xml:id="B05624-001-a-2830">,</pc>
     <w lemma="you" pos="pn" xml:id="B05624-001-a-2840">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05624-001-a-2850">pass</w>
     <w lemma="to" pos="acp" xml:id="B05624-001-a-2860">to</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2870">the</w>
     <w lemma="mercat-cross" pos="n1" xml:id="B05624-001-a-2880">Mercat-Cross</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2890">of</w>
     <hi xml:id="B05624-e230">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05624-001-a-2900">Edinburgh</w>
      <pc xml:id="B05624-001-a-2910">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-2920">and</w>
     <w lemma="to" pos="acp" xml:id="B05624-001-a-2930">to</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2940">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05624-001-a-2950">Mercat-Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-2960">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-2970">the</w>
     <w lemma="remanent" pos="fla" xml:id="B05624-001-a-2980">remanent</w>
     <w lemma="headburgh" pos="n2" xml:id="B05624-001-a-2990">Head-burghs</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3000">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-3010">the</w>
     <w lemma="several" pos="j" xml:id="B05624-001-a-3020">several</w>
     <w lemma="shire" pos="n2" xml:id="B05624-001-a-3030">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3040">of</w>
     <w lemma="this" pos="d" xml:id="B05624-001-a-3050">this</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-3060">Our</w>
     <w lemma="ancient" pos="j" reg="Ancient" xml:id="B05624-001-a-3070">Antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05624-001-a-3080">Kingdom</w>
     <pc xml:id="B05624-001-a-3090">,</pc>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-3100">and</w>
     <w lemma="there" pos="av" xml:id="B05624-001-a-3110">there</w>
     <w lemma="by" pos="acp" xml:id="B05624-001-a-3120">by</w>
     <w lemma="open" pos="j" xml:id="B05624-001-a-3130">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05624-001-a-3140">Proclamation</w>
     <pc xml:id="B05624-001-a-3150">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05624-001-a-3160">make</w>
     <w lemma="intimation" pos="n1" xml:id="B05624-001-a-3170">Intimation</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3180">of</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-3190">the</w>
     <w lemma="adjourn" pos="vvg" xml:id="B05624-001-a-3200">Adjourning</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3210">of</w>
     <w lemma="our" pos="po" xml:id="B05624-001-a-3220">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05624-001-a-3230">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3240">of</w>
     <w lemma="this" pos="d" xml:id="B05624-001-a-3250">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05624-001-a-3260">Kingdom</w>
     <pc xml:id="B05624-001-a-3270">,</pc>
     <w lemma="from" pos="acp" xml:id="B05624-001-a-3280">from</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-3290">the</w>
     <w lemma="say" pos="vvd" xml:id="B05624-001-a-3300">said</w>
     <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-3310">fifteenth</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-3320">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3330">of</w>
     <hi xml:id="B05624-e240">
      <w lemma="march" pos="n1" xml:id="B05624-001-a-3340">March</w>
     </hi>
     <w lemma="insant" pos="ffr" xml:id="B05624-001-a-3350">insant</w>
     <pc xml:id="B05624-001-a-3360">,</pc>
     <w lemma="to" pos="acp" xml:id="B05624-001-a-3370">to</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-3380">the</w>
     <w lemma="say" pos="vvd" xml:id="B05624-001-a-3390">said</w>
     <w lemma="fifteen" pos="ord" xml:id="B05624-001-a-3400">fifteenth</w>
     <w lemma="day" pos="n1" xml:id="B05624-001-a-3410">day</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-3420">of</w>
     <hi xml:id="B05624-e250">
      <w lemma="June" pos="nn1" xml:id="B05624-001-a-3430">June</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="B05624-001-a-3440">next</w>
     <w lemma="to" pos="prt" xml:id="B05624-001-a-3450">to</w>
     <w lemma="come" pos="vvi" xml:id="B05624-001-a-3460">come</w>
     <pc unit="sentence" xml:id="B05624-001-a-3470">.</pc>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-3480">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05624-001-a-3490">Ordains</w>
     <w lemma="these" pos="d" xml:id="B05624-001-a-3500">these</w>
     <w lemma="present" pos="n2" xml:id="B05624-001-a-3510">Presents</w>
     <w lemma="to" pos="prt" xml:id="B05624-001-a-3520">to</w>
     <w lemma="be" pos="vvi" xml:id="B05624-001-a-3530">be</w>
     <w lemma="print" pos="vvn" xml:id="B05624-001-a-3540">Printed</w>
     <pc unit="sentence" xml:id="B05624-001-a-3550">.</pc>
    </p>
    <closer xml:id="B05624-e260">
     <dateline xml:id="B05624-e270">
      <w lemma="give" pos="vvn" xml:id="B05624-001-a-3560">Given</w>
      <w lemma="under" pos="acp" xml:id="B05624-001-a-3570">under</w>
      <w lemma="our" pos="po" xml:id="B05624-001-a-3580">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05624-001-a-3590">Signet</w>
      <w lemma="at" pos="acp" xml:id="B05624-001-a-3600">at</w>
      <hi xml:id="B05624-e280">
       <w lemma="Edinburgh" pos="nn1" xml:id="B05624-001-a-3610">Edinburgh</w>
       <pc xml:id="B05624-001-a-3620">,</pc>
      </hi>
      <date xml:id="B05624-e290">
       <w lemma="the" pos="d" xml:id="B05624-001-a-3630">the</w>
       <w lemma="twelve" pos="ord" xml:id="B05624-001-a-3640">twelfth</w>
       <w lemma="day" pos="n1" xml:id="B05624-001-a-3650">day</w>
       <w lemma="of" pos="acp" xml:id="B05624-001-a-3660">of</w>
       <hi xml:id="B05624-e300">
        <w lemma="march" pos="n1" xml:id="B05624-001-a-3670">March</w>
        <pc xml:id="B05624-001-a-3680">,</pc>
       </hi>
       <w lemma="and" pos="cc" xml:id="B05624-001-a-3690">and</w>
       <w lemma="of" pos="acp" xml:id="B05624-001-a-3700">of</w>
       <w lemma="our" pos="po" xml:id="B05624-001-a-3710">our</w>
       <w lemma="reign" pos="n1" xml:id="B05624-001-a-3720">Reign</w>
       <w lemma="the" pos="d" xml:id="B05624-001-a-3730">the</w>
       <w lemma="five" pos="ord" xml:id="B05624-001-a-3740">Fifth</w>
       <w lemma="year" pos="n1" xml:id="B05624-001-a-3750">Year</w>
       <pc xml:id="B05624-001-a-3760">,</pc>
       <hi xml:id="B05624-e310">
        <w lemma="1694" pos="crd" xml:id="B05624-001-a-3770">1694</w>
       </hi>
      </date>
      <pc unit="sentence" xml:id="B05624-001-a-3780">.</pc>
     </dateline>
     <signed xml:id="B05624-e320">
      <w lemma="per" pos="fla" xml:id="B05624-001-a-3790">Per</w>
      <w lemma="actum" pos="fla" xml:id="B05624-001-a-3800">Actum</w>
      <w lemma="dominorum" pos="fla" xml:id="B05624-001-a-3810">Dominorum</w>
      <w lemma="secreti" pos="fla" xml:id="B05624-001-a-3820">Secreti</w>
      <w lemma="concilii" pos="fla" xml:id="B05624-001-a-3830">Concilii</w>
      <pc unit="sentence" xml:id="B05624-001-a-3840">.</pc>
      <hi xml:id="B05624-e330">
       <w lemma="GILB" pos="nn1" xml:id="B05624-001-a-3850">GILB</w>
       <pc unit="sentence" xml:id="B05624-001-a-3860">.</pc>
       <w lemma="eliot" pos="nn1" reg="eliot" xml:id="B05624-001-a-3870">ELLIOT</w>
      </hi>
      <w lemma="Cls." pos="nn1" xml:id="B05624-001-a-3880">Cls.</w>
      <w lemma="sti." pos="ab" xml:id="B05624-001-a-3890">Sti.</w>
      <w lemma="concilii" pos="fla" xml:id="B05624-001-a-3900">Concilii</w>
      <pc unit="sentence" xml:id="B05624-001-a-3910">.</pc>
     </signed>
     <lb xml:id="B05624-e340"/>
     <w lemma="God" pos="nn1" xml:id="B05624-001-a-3920">God</w>
     <w lemma="save" pos="acp" xml:id="B05624-001-a-3930">Save</w>
     <w lemma="king" pos="n1" xml:id="B05624-001-a-3940">King</w>
     <hi xml:id="B05624-e350">
      <w lemma="William" pos="nn1" xml:id="B05624-001-a-3950">William</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05624-001-a-3960">and</w>
     <w lemma="queen" pos="n1" xml:id="B05624-001-a-3970">Queen</w>
     <hi xml:id="B05624-e360">
      <w lemma="Mary" pos="nn1" xml:id="B05624-001-a-3980">Mary</w>
      <pc unit="sentence" xml:id="B05624-001-a-3990">.</pc>
     </hi>
    </closer>
   </div>
  </body>
  <back xml:id="B05624-e370">
   <div type="colophon" xml:id="B05624-e380">
    <p xml:id="B05624-e390">
     <hi xml:id="B05624-e400">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05624-001-a-4000">Edinburgh</w>
      <pc xml:id="B05624-001-a-4010">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B05624-001-a-4020">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05624-001-a-4030">by</w>
     <w lemma="the" pos="d" xml:id="B05624-001-a-4040">the</w>
     <w lemma="successor" pos="n2" xml:id="B05624-001-a-4050">Successors</w>
     <w lemma="of" pos="acp" xml:id="B05624-001-a-4060">of</w>
     <hi xml:id="B05624-e410">
      <w lemma="Andrew" pos="nn1" xml:id="B05624-001-a-4070">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05624-001-a-4080">Anderson</w>
      <pc xml:id="B05624-001-a-4090">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05624-001-a-4100">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05624-001-a-4110">to</w>
     <w lemma="their" pos="po" xml:id="B05624-001-a-4120">Their</w>
     <w lemma="most" pos="avs-d" xml:id="B05624-001-a-4130">Most</w>
     <w lemma="excellent" pos="j" xml:id="B05624-001-a-4140">Excellent</w>
     <w lemma="majcesty" pos="n2" xml:id="B05624-001-a-4150">Majcesties</w>
     <pc xml:id="B05624-001-a-4160">,</pc>
     <hi xml:id="B05624-e420">
      <w lemma="anno" pos="fla" xml:id="B05624-001-a-4170">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05624-001-a-4180">Dom.</w>
     </hi>
     <w lemma="1694." pos="crd" xml:id="B05624-001-a-4190">1694.</w>
     <pc unit="sentence" xml:id="B05624-001-a-4200"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
